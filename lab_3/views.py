from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def index(request):
    return render(request, 'lab3templates.html')

def about(request):
    return render(request, 'about.html')

def riwayatOrganisasi(request):
    return render(request, 'riwayat organisasi.html')

def riwayatPendidikan(request):
    return render(request, 'riwayat pendidikan.html')