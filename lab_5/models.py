from django.db import models

class JadwalPribadi(models.Model):
	nama = models.CharField(max_length=200)
	tempat = models.CharField(max_length=200)
	kategori = models.CharField(null = True, blank = True, max_length=150)
	waktu = models.DateTimeField()

	#displaying name object in django admin
	def __str__(self):
		return self.nama

class Feedback(models.Model):
	nama = models.CharField(max_length=200)
	saran = models.TextField()

	def __str__(self):
		return self.nama


