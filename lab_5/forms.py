from django import forms
from .models import JadwalPribadi, Feedback

class Add_Project_Form(forms.ModelForm):

    class Meta:
        model = JadwalPribadi
        fields = ['nama', 'tempat', 'kategori', 'waktu',]
   
    err_msg = [
        'Panjang field maksimal 200 karakter',
        'Panjang field maksimal 150 karakter',
        'Masukkan sesuai input yang tertera',
    ]


    nama_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Masukkan Nama Kegiatan'
    }

    tempat_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Masukkan Nama Tempat'
    }

    kategori_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Masukkan Nama Kategori'
    }

    waktu_attrs = {
        'type':'datetime-local',
        'class' : 'form-control'
    }

    nama = forms.CharField(required=True, widget=forms.TextInput(attrs=nama_attrs), error_messages={'max_length':err_msg[0]})
    tempat = forms.CharField(required=True, widget=forms.TextInput(attrs=tempat_attrs), error_messages={'max_length':err_msg[0]})
    kategori = forms.CharField(required=False,widget=forms.TextInput(attrs=kategori_attrs),error_messages={'max_length':err_msg[1]})
    waktu = forms.DateTimeField(required=True, widget=forms.DateTimeInput(attrs=waktu_attrs),input_formats=['%Y-%m-%dT%H:%M'])

class Add_Feedback_Form(forms.Form):

    class meta:
        model = JadwalPribadi
        fields = ['nama', 'saran']

    nama_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Namamu (inisial juga boleh)',
    }

    saran_attrs = {
        'type': 'text',
        'row' : 4,
        'cols' : 50,
        'class': 'form-control',
        'placeholder':'Masukkan saran',
    }

    nama = forms.CharField(required=True, max_length=50, widget=forms.TextInput(attrs=nama_attrs))
    saran = forms.CharField(required=True, widget=forms.Textarea(attrs=saran_attrs))

