from django.contrib import admin

from .models import JadwalPribadi, Feedback

admin.site.register(JadwalPribadi)
admin.site.register(Feedback)
