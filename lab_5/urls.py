from django.urls import re_path
from .views import ScheduleList, ScheduleCreate, addfeedback, savefeedback, delete_all
#url for app

urlpatterns = [
    re_path(r'^jadwal/create/$', ScheduleCreate.as_view(), name='jadwal-form'),
    re_path(r'^jadwal/$', ScheduleList.as_view(), name='jadwal-list'),
    re_path(r'^feedback/$', addfeedback , name='feedback-form'),
    re_path(r'^feedback/save/$', savefeedback, name='feedback-save'),
    re_path(r'^jadwal/detelealldatabase/$', delete_all, name='delete-database')
]
