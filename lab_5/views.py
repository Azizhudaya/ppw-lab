from django.shortcuts import reverse, render, redirect
from django.http import HttpResponseRedirect
from .models import JadwalPribadi, Feedback
from .forms import Add_Project_Form, Add_Feedback_Form
from django.views.generic import CreateView, ListView



class ScheduleList(ListView):
    model = JadwalPribadi
    template_name = 'list_form.html'
    context_object_name = 'jadwal'
    queryset = JadwalPribadi.objects.all()

class ScheduleCreate(CreateView):
    model = JadwalPribadi
    form_class = Add_Project_Form
    template_name = 'form_registration.html'

    def get_success_url(self):
        return reverse('form:jadwal-list')
 
def delete_all(request):
	if request.method == "POST":
	    list = JadwalPribadi.objects.all().delete()
	    return redirect('form:jadwal-list')
	else:
		return redirect('form:jadwal-list')

response = {}
def addfeedback(request):

	response['title'] = 'Add Project'
	response['feedback_form'] = Add_Feedback_Form

	html = 'feedback.html'
	return render(request, html, response)

def savefeedback(request):
	form = Add_Feedback_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['nama'] = request.POST['nama']
		response['saran'] = request.POST['saran']
		feedback = Feedback(nama=response['nama'], saran=response['saran'])
		feedback.save()

		return HttpResponseRedirect("/")

	else:

		return HttpResponseRedirect("/")







